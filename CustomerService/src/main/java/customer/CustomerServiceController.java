package customer;

import java.util.Calendar;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

import javax.annotation.PostConstruct;

import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class CustomerServiceController implements CustomerService{

	private Map<String, Customer> db;

	@PostConstruct
	public void init() {
		this.db = Collections.synchronizedMap(new TreeMap<>());
	}
	
	@Override
	@RequestMapping(path = "/customers", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	public Customer createCustomer(@RequestBody Customer newCustomer) {
		if(this.db.containsKey(newCustomer.getEmail())) {
			throw new CreateUserException("Your Email is already in use! Try again");
		}
		else if(!emailChecker(newCustomer.getEmail())) {
			throw new CreateUserException("Email is not in good format! Try again");
		}
		else if(newCustomer.getName().getFirst().isEmpty() || newCustomer.getName().getLast().isEmpty()) {
			throw new CreateUserException("Name must include First name and Last name not empty! Try again");
		}
		else if(newCustomer.getCountry().length > 2) {
			throw new CreateUserException("Country must be 2 letters representing country code! Try again");
		}
		else if(newCustomer.getPassword().length() < 3) {
			throw new CreateUserException("Password must be 3 or more letters! Try again");
		}
		
		this.db.put(newCustomer.getEmail(), newCustomer);
		return this.db.get(newCustomer.getEmail());
	}

	@Override
	@RequestMapping(path = "/customers", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public List<Customer> getAllCustomers(
			@RequestParam(name="size", required = false, defaultValue = "20") int size,
			@RequestParam(name="page", required = false, defaultValue = "0") int page,
			@RequestParam(name="byLastName", required = false, defaultValue = "") String lastname,
			@RequestParam(name="byLastNameBeginsWith", required = false, defaultValue = "") String lastnameBeginning,
			@RequestParam(name="byCountry", required = false, defaultValue = "") String country,
			@RequestParam(name="byAgeOver", required = false, defaultValue = "-1") int minAgeExclusive) {
		
		if(!lastname.isEmpty()) {
			if(!lastnameBeginning.isEmpty() || !country.isEmpty() || minAgeExclusive !=-1) {
				throw new illegalInputException("You type an illegal path! Try again");
			} else {
				return
				this.db
				.values()
				.stream()
				.skip(size * page)
				.limit(size)
				.filter(c -> c.getName().getLast().equalsIgnoreCase(lastname))
				.collect(Collectors.toList());
			}
		}
		
		else if(!lastnameBeginning.isEmpty()) {
			if(!lastname.isEmpty() || !country.isEmpty() || minAgeExclusive !=-1) {
				throw new illegalInputException("You type an illegal path! Try again");
			} else {
				return
				this.db
				.values()
				.stream()
				.skip(size * page)
				.limit(size)
				.filter(c -> c.getName().getLast().toLowerCase().startsWith(lastnameBeginning.toLowerCase()))
				.collect(Collectors.toList());
			}
		}
		
		else if(!country.isEmpty()) {
			if(country.length() != 2 || !lastname.isEmpty() || !lastnameBeginning.isEmpty() || minAgeExclusive !=-1) {
				throw new illegalInputException("You type an illegal path! Try again");
			} else {
				return
				this.db
				.values()
				.stream()
				.skip(size * page)
				.limit(size)
				.filter(c ->
						Character.toLowerCase(c.getCountry()[0]) == Character.toLowerCase(country.charAt(0))
						&& Character.toLowerCase(c.getCountry()[1]) == Character.toLowerCase(country.charAt(1)))
				.collect(Collectors.toList());
			}
		}
		else if(minAgeExclusive > 0) {
			if(!lastname.isEmpty() || !lastnameBeginning.isEmpty() || !country.isEmpty()) {
				throw new illegalInputException("You type an illegal path! Try again");
			} else {
				return
				this.db
				.values()
				.stream()
				.skip(size * page)
				.limit(size)
				.filter(c ->{
					Calendar cal = Calendar.getInstance();
					cal.setTime(c.getBirthdate());
					return minAgeExclusive < Calendar.getInstance().get(Calendar.YEAR) - cal.get(Calendar.YEAR);
				})
				.collect(Collectors.toList());
			}
		}
		return
			this.db
			.values()
			.stream()
			.skip(size * page)
			.limit(size)
			.collect(Collectors.toList());
	}

	@Override
	@RequestMapping(path = "/customers/{email}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public Customer getCustomer(@PathVariable("email") String email) {
		if(!this.db.containsKey(email)) {
			throw new illegalInputException("Email not found! Try again");
		} else {
			return this.db.get(email);
		}
	}
	
	@Override
	@RequestMapping(path = "/customers/{email}/{password}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public Customer loginCustomer (
			@PathVariable("email") String email,
			@PathVariable("password") String password) {
		if(!this.db.containsKey(email)) {
			throw new illegalInputException("Email not found! Try again");
		}
		else if(!this.db.get(email).getPassword().equals(password)) {
			throw new illegalInputException("Wrong Email or Password! Try again");
		}
		else {
			return this.db.get(email);
		}
	}

	@Override
	@RequestMapping(path = "/customers/{email}", method = RequestMethod.PUT, 
	consumes = MediaType.APPLICATION_JSON_VALUE)
	public void update(
			@PathVariable("email")String email,
			@RequestBody Customer update) {
		if(!this.db.containsKey(email)) {
			throw new illegalInputException("Email not found! Try again");
		} else {
			update.setEmail(email);
			this.db.put(email, update);
		}
	}

	@Override
	@RequestMapping(path = "/customers", method = RequestMethod.DELETE)
	public void deleteAll() {
		this.db.clear();
	}

	@ExceptionHandler
	@ResponseStatus(code = HttpStatus.NOT_FOUND)
	public Map<String, String> handleIllegalInput (illegalInputException e){
		String message = e.getMessage();
		if (message == null) {
			message = "Customer not found";
		}
		return Collections.singletonMap("error", message);
	}
	
	public boolean emailChecker(String email) {
		String regex = "^[\\w!#$%&'*+/=?`{|}~^-]+(?:\\.[\\w!#$%&'*+/=?`{|}~^-]+)*@(?:[a-zA-Z0-9-]+\\.)+[a-zA-Z]{2,6}$";
		Pattern pattern = Pattern.compile(regex);
		Matcher matcher = pattern.matcher(email);
		return matcher.matches();
	}
	
}
