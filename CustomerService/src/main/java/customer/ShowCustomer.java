package customer;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonFormat;

public class ShowCustomer {
	
	public class Name {
		private String first;
		private String last;
		
		public Name() {
		}

		public String getFirst() {
			return first;
		}

		public void setFirst(String first) {
			this.first = first;
		}

		public String getLast() {
			return last;
		}

		public void setLast(String last) {
			this.last = last;
		}

	}

	private String email;
	private Name name;
	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd-MM-yyyy")
	private Date birthdate;
	private char[] country;
	
	
	public ShowCustomer() {
	}
	
	public ShowCustomer(Customer c) {
		this.email = c.getEmail();
		this.name = new Name();
		this.name.setFirst(c.getName().getFirst());
		this.name.setLast(c.getName().getLast());
		this.birthdate = c.getBirthdate();
		this.country = c.getCountry();
	}


	public String getEmail() {
		return email;
	}


	public void setEmail(String email) {
		this.email = email;
	}


	public Name getName() {
		return name;
	}


	public void setName(Name name) {
		this.name = name;
	}


	public Date getBirthdate() {
		return birthdate;
	}


	public void Date(Date birthdate) {
		this.birthdate = birthdate;
	}


	public char[] getCountry() {
		return country;
	}


	public void setCountry(char[] country) {
		this.country = country;
	}
		
}
