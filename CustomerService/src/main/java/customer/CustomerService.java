package customer;

import java.util.List;

public interface CustomerService {
	
	public Customer createCustomer (Customer newCustomer);
	public List<Customer> getAllCustomers (int size, int page, String lastname, String lastnameBeginning, String country, int minAgeExclusive);
	public Customer getCustomer (String email);
	public Customer loginCustomer (String email, String password);
	public void update (String email, Customer update);
	public void deleteAll ();
	
}
