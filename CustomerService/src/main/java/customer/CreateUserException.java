package customer;

public class CreateUserException extends RuntimeException {

	public CreateUserException() {
	}

	public CreateUserException(String message) {
		super(message);
	}

}
